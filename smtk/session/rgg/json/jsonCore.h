//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_rgg_json_jsonCore_h
#define smtk_session_rgg_json_jsonCore_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/session/rgg/Core.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace rgg
{
SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const Core& core);

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, Core& core);

} // namespace rgg
} // namespace session
} // namespace smtk
#endif
