//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkRGGEditCoreView - UI component for Edit RGG Core
// .SECTION Description
// .SECTION See Also

#ifndef smtkRGGEditCoreView_h
#define smtkRGGEditCoreView_h

#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/session/rgg/operators/CreateModel.h"
#include "smtk/session/rgg/plugin/Exports.h"

class QColor;
class QComboBox;
class QIcon;
class QTableWidget;

namespace smtk
{
namespace extension
{
class qtItem;
}
}

class smtkRGGEditCoreViewInternals;

class smtkRGGEditCoreView : public smtk::extension::qtBaseAttributeView
{
  Q_OBJECT

public:
  smtkTypenameMacro(smtkRGGEditCoreView);
  smtkRGGEditCoreView(const smtk::extension::OperationViewInfo& info);
  virtual ~smtkRGGEditCoreView() override;

  static smtk::extension::qtBaseView* createViewWidget(const smtk::extension::ViewInfo& info);

  bool displayItem(smtk::attribute::ItemPtr) override;

public slots:
  void requestModelEntityAssociation() override;
  void onShowCategory() override { this->updateUI(); }
  void valueChanged(smtk::attribute::ItemPtr optype) override;

protected slots:
  virtual void requestOperation(const smtk::operation::OperationPtr& op);

  // This slot is used to indicate that the underlying attribute
  // for the operation should be checked for validity
  virtual void attributeModified();
  void onAttItemModified(smtk::extension::qtItem* item);
  void apply();

  void launchSchemaPlanner();

protected:
  void updateUI() override;
  void createWidget() override;
  // When the core item has been modified, this function would populate the edit
  // core panel
  void updateEditCorePanel();
  virtual void setInfoToBeDisplayed() override;

private:
  smtkRGGEditCoreViewInternals* Internals;
};

#endif // smtkRGGEditCoreView_h
