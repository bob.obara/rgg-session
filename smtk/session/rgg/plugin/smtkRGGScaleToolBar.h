//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtkRGGScaleToolBar_h
#define smtkRGGScaleToolBar_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/session/rgg/plugin/Exports.h"

#include <QToolBar>

class pqView;
class QAction;
class QSlider;

// This class provides a toolbar for user to scale the rgg entities along z axis.
class smtkRGGScaleToolBar : public QToolBar
{
  Q_OBJECT
  using Superclass = QToolBar;

public:
  smtkRGGScaleToolBar(QWidget* parent = nullptr);
  ~smtkRGGScaleToolBar() override;
public Q_SLOTS:
  void onValueChanged(int v);
  void onViewChanged(pqView* v);

private:
  Q_DISABLE_COPY(smtkRGGScaleToolBar);
  QSlider* m_scaleSlider;
};

#endif
