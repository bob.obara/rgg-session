<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "generate_mesh" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="generate mesh" Label="Model - Generate Mesh" BaseType="operation">
      <BriefDescription>
        Generate a mesh from an RGG model using MeshKit + Cubit
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Generate a mesh from an RGG model using MeshKit + Cubit.
      </DetailedDescription>
      <AssociationsDef LockType="Read">
        <Accepts><Resource Name="smtk::session::rgg::Resource" Filter="model"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="assygen" Label="Assygen Executable" NumberOfRequiredValues="1"
              ShouldExist="true" Optional="true"
              IsEnabledByDefault="false" AdvanceLevel="1"/>
        <File Name="coregen" Label="Coregen Executable" NumberOfRequiredValues="1"
              ShouldExist="true" Optional="true"
              IsEnabledByDefault="false" AdvanceLevel="1"/>
        <File Name="cubit" Label="Cubit Executable" NumberOfRequiredValues="1"
              ShouldExist="true" Optional="true"
              IsEnabledByDefault="false" AdvanceLevel="1"/>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(generate mesh)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
      </AttDef>
  </Definitions>
</SMTK_AttributeResource>
