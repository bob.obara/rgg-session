<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "ExportInp" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export inp" Label="Model - Export Inp" BaseType="operation">
      <BriefDescription>Export a rgg core to inp files</BriefDescription>
      <DetailedDescription>
        Given a path, this operator wil export the rgg core and its assemblies
        into inp files. It will also write a common.inp file which will be used
        by AssyGen to load common parameters used by all assemblies.
      </DetailedDescription>
      <AssociationsDef Name="model" NumberOfRequiredValues="1"
                       AdvanceLevel="0" LockType="Read">
        <Accepts>
          <Resource Name="smtk::session::rgg::Resource" Filter="model"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Directory Name="directory" NumberOfRequiredValues="1" AdvanceLevel="0">
          <BriefDescription>directory to export the nuclear core into inp files</BriefDescription>
          <DetailedDescription>
            A user provided directory to export inp files.
          </DetailedDescription>
        </Directory>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(edit duct)" BaseType="result">
      <ItemDefinitions>
        <Component Name="tess_changed" NumberOfRequiredValues="0" Extensible="true"/>
        <!-- The edit duct is returned in the base result's "edit" item. -->
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
     <!--
      The customized view "Type" needs to match the plugin's class
      -->
    <View Type="smtkRGGEditDuctView" Title="Edit Duct"  FilterByCategory="false"  FilterByAdvanceLevel="false" UseSelectionManager="false">
      <Description>
        A view for changing a nuclear core's properties and layout.
      </Description>
      <AttributeTypes>
        <Att Type="edit duct"/>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
