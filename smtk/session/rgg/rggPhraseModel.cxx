//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/rggPhraseModel.h"

#include "smtk/view/ComponentPhraseContent.h"
#include "smtk/view/DescriptivePhrase.h"
#include "smtk/view/QueryFilterSubphraseGenerator.h"
#include "smtk/view/ObjectGroupPhraseContent.h"
#include "smtk/view/PhraseListContent.h"

#include "smtk/operation/Manager.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"

#include "smtk/resource/Component.h"

#include <algorithm> // for std::sort

namespace smtk
{
namespace session
{
namespace rgg
{

rggPhraseModel::rggPhraseModel()
  : m_root(smtk::view::DescriptivePhrase::create())
{
}

rggPhraseModel::~rggPhraseModel()
{
  this->resetSources();
}

smtk::view::PhraseModelPtr rggPhraseModel::create(const smtk::view::Configuration::Component& itemViewSpec)
{
  // According to David, currently the itemViewSpec is not used but in the future it wll contain information to
  // customize how the model should behave
  (void)itemViewSpec;
  auto model = PhraseModel::Ptr(new rggPhraseModel);
  model->root()->findDelegate()->setModel(model);
  return model;
}


smtk::view::DescriptivePhrasePtr rggPhraseModel::root() const
{
  return m_root;
}

void rggPhraseModel::handleResourceEvent(const Resource& rsrc, smtk::resource::EventType event)
{
  if (event == smtk::resource::EventType::ADDED)
  {
    this->processResource(rsrc, true);
  }
}

void rggPhraseModel::handleCreated(
  const Operation& op, const Operation::Result& res, const ComponentItemPtr& data)
{
  (void)op;
  if (!res || !data)
  {
    return;
  }

  // TODO: Instead of looking for new resources (which perhaps we should leave to the
  //       handleResourceEvent()), we should optimize by adding new components to the
  //       root phrase if they pass m_componentFilters.
  for (auto it = data->begin(); it != data->end(); ++it)
  {
    auto comp = std::dynamic_pointer_cast<smtk::resource::Component>(*it);
    if (comp == nullptr)
    {
      continue;
    }
    smtk::resource::ResourcePtr rsrc = comp->resource();
    this->processResource(*rsrc, true);
  }

  // Finally, call our subclass method to deal with children of the root element
  this->PhraseModel::handleCreated(op, res, data);
}

void rggPhraseModel::processResource(const Resource& rsrc, bool adding)
{
  if (adding)
  {
    this->populateRoot();
  }
  else
  {
    // FIXME: Visit all, not immediate, children.
    smtk::view::DescriptivePhrases children(m_root->subphrases());
    children.erase(std::remove_if(children.begin(), children.end(),
        [&rsrc](const smtk::view::DescriptivePhrase::Ptr& phr) -> bool {
        return phr->relatedResource()->id() == rsrc.id();
        }),
      children.end());
    this->updateChildren(m_root, children, std::vector<int>());
  }
}

void rggPhraseModel::populateRoot()
{
  const std::string rsrcFilt("smtk::session::rgg::Resource");
  constexpr int typeNum = 4;
  const char* compFilts[typeNum][2] = {
    { "group[string{'selectable'='_rgg_core'}]", "core" },
    { "group[string{'selectable'='_rgg_assembly'}]", "assemblies" },
    { "aux_geom[string{'selectable'='_rgg_pin'}]", "pins" },
    { "aux_geom[string{'selectable'='_rgg_duct'}]", "ducts" }
  };

  smtk::view::DescriptivePhrases children;
  for (int ii = 0; ii < typeNum; ++ii)
  {
    children.emplace_back(
      smtk::view::ObjectGroupPhraseContent::createPhrase(
        compFilts[ii][1], rsrcFilt, compFilts[ii][0], m_root));
  }
  this->root()->findDelegate()->decoratePhrases(children);
  this->updateChildren(m_root, children, std::vector<int>());
}

}
}
}
