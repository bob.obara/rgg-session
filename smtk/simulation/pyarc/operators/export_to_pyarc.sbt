<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "export_to_pyarc" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export to pyarc" Label="Model - Export to PyARC" BaseType="operation">
      <BriefDescription>
        Export an RGG model as PyARC geometry
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Export an RGG model as PyARC geometry.
      </DetailedDescription>
      <AssociationsDef Name="model" NumberOfRequiredValues="1" AdvanceLevel="0" LockType="Read">
        <MembershipMask>model</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" Label="Output File" NumberOfRequiredValues="1" ShouldExist="false"
          FileFilters="PyARC SON file (*.son)">
        </File>
        <Component Name="mcc3" Label="MCC3 Description" Optional="True" IsEnabledByDefault="true">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='mcc3']"/>
          </Accepts>
        </Component>
        <Component Name="dif3d" Label="DIF3D Description"
                   Optional="True" IsEnabledByDefault="false">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='dif3d']"/>
          </Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(export to pyarc)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
