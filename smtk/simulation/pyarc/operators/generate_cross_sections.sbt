<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the RGG "generate_cross_sections" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="generate cross sections with pyarc" Label="Model - Generate Cross Sections with PyARC" BaseType="operation">
      <BriefDescription>
        Generate a cross-section file from an RGG model using PyARC + mcc3
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Generate a cross-section file from an RGG model using PyARC + mcc3.
      </DetailedDescription>
      <AssociationsDef LockType="Read">
        <Accepts><Resource Name="smtk::session::rgg::Resource" Filter="model"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Component Name="mcc3_atts" Label="MCC3 input attribute Resource" LockType="Read">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='mcc3']"/>
          </Accepts>
        </Component>
        <File Name="mcc3.x" Label="MCC3 Executable" NumberOfRequiredValues="1"
              ShouldExist="true" Optional="true"
              IsEnabledByDefault="false" AdvanceLevel="1"/>
        <File Name="filename" Label="Output File" NumberOfRequiredValues="1"/>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(generate cross sections)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
