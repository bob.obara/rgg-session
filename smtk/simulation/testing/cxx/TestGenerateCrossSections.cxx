//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/io/AttributeReader.h"

#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/Core.h"
#include "smtk/session/rgg/Pin.h"
#include "smtk/session/rgg/Registrar.h"
#include "smtk/session/rgg/Resource.h"
#include "smtk/session/rgg/Session.h"

#include "smtk/session/rgg/operators/AddMaterial.h"
#include "smtk/session/rgg/operators/EditAssembly.h"
#include "smtk/session/rgg/operators/EditCore.h"
#include "smtk/session/rgg/operators/EditDuct.h"
#include "smtk/session/rgg/operators/EditPin.h"
#include "smtk/session/rgg/operators/ExportInp.h"
#include "smtk/session/rgg/operators/ReadRXFFile.h"

#include "smtk/session/rgg/json/jsonAssembly.h"
#include "smtk/session/rgg/json/jsonCore.h"
#include "smtk/session/rgg/json/jsonDuct.h"
#include "smtk/session/rgg/json/jsonPin.h"

#include "smtk/simulation/proteus/Registrar.h"

#include "smtk/mesh/core/Resource.h"
#include "smtk/mesh/resource/Registrar.h"

#include "smtk/model/EntityRef.h"
#include "smtk/model/Group.h"
#include "smtk/model/Model.h"

#include "smtk/session/rgg/testing/cxx/TestModel.h"

using namespace smtk::session::rgg;
using json = nlohmann::json;

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace
{
std::string dataRoot = DATA_DIR;
std::string writeRoot = SCRATCH_DIR;
std::string mcc3Exe = MCC3_EXE;

bool exists(std::string filename)
{
  return ::boost::filesystem::exists(::boost::filesystem::path(filename));
}

bool findExecutables()
{
  //verify the executables we need to generate a mesh exist
  return exists(mcc3Exe);
}

void cleanup(const std::string& file_path)
{
  //first verify the file exists
  ::boost::filesystem::path path(file_path);
  if (::boost::filesystem::is_regular_file(path))
  {
    //remove the file_path if it exists.
    ::boost::filesystem::remove(path);
  }
}
}

int TestGenerateCrossSections(int, char**)
{
  if (findExecutables() == false)
  {
    std::cerr << "Could not locate the necessary executables to run this test.\n";
    std::cerr << "Please set MCC3_EXE during configuration.\n";
    std::cerr << "Current values:" << "\n";
    std::cerr << "  MCC3_EXE = " << mcc3Exe << "\n";
    return 125;
  }

  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register rgg resources to the resource manager
  {
    smtk::attribute::Registrar::registerTo(resourceManager);
    smtk::session::rgg::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register rgg operators to the operation manager
  {
    smtk::session::rgg::Registrar::registerTo(operationManager);
    smtk::simulation::proteus::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  // Construct an RGG model
  smtk::model::ResourcePtr resource = smtk::session::rgg::test::constructTestModel();
  smtk::model::Model model =
    resource->entitiesMatchingFlagsAs<smtk::model::EntityRefArray>(smtk::model::MODEL_ENTITY)[0];

  // Construct PYARC simulation attributes for MCC3 and DIF3D
  smtk::attribute::ResourcePtr pyarcSimulationAttributes;
  smtk::attribute::AttributePtr mcc3Att;
  {
    pyarcSimulationAttributes = resourceManager->create<smtk::attribute::Resource>();

    smtk::io::Logger logger;
    smtk::io::AttributeReader reader;
    // the attribute reader returns true on failure...
    if (reader.read(pyarcSimulationAttributes,
                    dataRoot + "/../smtk/simulation/pyarc/templates/mcc3.sbt", true, logger))
    {
      std::cerr << "Import PYARC simulation attributes failed\n";
      std::cerr << logger.convertToString(true) << "\n";
      return 1;
    }

    mcc3Att = pyarcSimulationAttributes->createAttribute("mcc3-instance", "mcc3");

    if (!mcc3Att->isValid())
    {
      std::cerr << "Failed to set dummy mcc3 values\n";
      return 1;
    }
  }

  // Create a "Generate Cross Sections" operation
  smtk::operation::Operation::Ptr generateCrossSectionsOp =
    operationManager->create("rggsession.simulation.proteus.generate_cross_sections.GenerateCrossSections");

  if (!generateCrossSectionsOp)
  {
    std::cerr << "Could not create \"generate cross sections\" operation\n";
    return 1;
  }

  std::string writeFilePath(writeRoot);
  writeFilePath += "/crosssections.isotxs";

  generateCrossSectionsOp->parameters()->associate(model.component());

  generateCrossSectionsOp->parameters()->findFile("mcc3.x")->setIsEnabled(true);
  generateCrossSectionsOp->parameters()->findFile("mcc3.x")->setValue(mcc3Exe);
  generateCrossSectionsOp->parameters()->findComponent("mcc3_atts")->setValue(mcc3Att);
  generateCrossSectionsOp->parameters()->findFile("filename")->setValue(writeFilePath);

  smtk::operation::Operation::Result generateCrossSectionsOpResult =
    generateCrossSectionsOp->operate();

  if (generateCrossSectionsOpResult->findInt("outcome")->value() !=
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED) || !exists(writeFilePath))
  {
    std::cerr << "\"generate cross sections\" operator failed to generate a cross sections file\n";
    std::cerr << generateCrossSectionsOp->log().convertToString(true) << "\n";
    return 1;
  }

  cleanup(writeFilePath);

  return 0;
}
